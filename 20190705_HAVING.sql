﻿USE ciclistas;

/*
  cuantas etapas ha ganado cada ciclista.
  dorsal y numEtapas
*/

SELECT
  dorsal,
  COUNT(*) numEtapas
FROM etapa e
GROUP BY e.dorsal
;

SELECT
  c.dorsal,
  c1.numEtapa
FROM ciclista c
  LEFT JOIN (SELECT
      e.dorsal,
      COUNT(*) numEtapa
    FROM etapa e
    GROUP BY e.dorsal) c1 USING (dorsal)
ORDER BY c.dorsal
;

/*
  Nombre de los ciclistas que han ganado mas de 2 etapas
*/

SELECT
  c.nombre
FROM ciclista c
  JOIN (SELECT
      e.dorsal
    FROM etapa e
    GROUP BY e.dorsal
    HAVING COUNT(*) > 2) c1 USING (dorsal)
;

/*
  El nombre del ciclista que tiene mas edad
*/
SELECT
  c.nombre
FROM
  ciclista c
WHERE
  c.edad = (SELECT MAX(edad) maximo FROM ciclista c1)
;

-- con JOIN
SELECT
  c.nombre
FROM
  ciclista c
  JOIN (SELECT MAX(edad) maximo FROM ciclista) c1 
  ON c.edad = c1.maximo
;