﻿USE ciclistas;

-- numero de etapas que hay
SELECT COUNT(*) nEtapas FROM etapa;

-- numero de maillots que hay
SELECT COUNT(*) nMaillots FROM maillot;

-- numero de ciclistas que hayan ganado alguna etapa
SELECT COUNT(DISTINCT dorsal) nGanadores FROM etapa;