﻿USE ciclistas;

/*
  ciclistas que han ganado etapas
  Mostrar dorsal
*/

SELECT
  DISTINCT e.dorsal
FROM
  etapa e
;

/*
  ciclistas que han ganado puertos
  Mostrar dorsal
*/

SELECT
  DISTINCT p.dorsal 
FROM
  puerto p
;

/* 
  Dorsal de los ciclistas que han ganado
  etapas Ó puertos
*/

SELECT DISTINCT e.dorsal FROM etapa e
  UNION
SELECT DISTINCT p.dorsal FROM puerto p
;

/* 
  Listado de todas las etapas
  y el ciclista que las ha ganado
*/

SELECT * FROM
  ciclista c JOIN etapa e
  ON c.dorsal = e.dorsal
;

/*
  Listado de todas los puertos
  y el ciclista que los ha ganado
*/

SELECT * FROM
  ciclista c JOIN puerto p
  ON c.dorsal = p.dorsal
;

/*
  Dorsal de los ciclistas que han ganado
  etapas Y puertos

SELECT DISTINCT e.dorsal FROM etapa e
  INTERSECT                                 -- NO EXISTE EN MySQL
SELECT DISTINCT p.dorsal FROM puerto p

  usaremos JOIN de la siguiente manera:
*/
SELECT c1.dorsalEtapa dorsal FROM 
  (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
    JOIN
  (SELECT DISTINCT p.dorsal dorsalPuerto FROM puerto p) c2
    ON c1.dorsalEtapa=c2.dorsalPuerto
;

/*
  Combinaciones interna
*/

-- Listar todos los ciclistas con todos los datos del equipo al que pertenecen
SELECT
  *
FROM equipo e JOIN ciclista c
    ON e.nomequipo = c.nomequipo
;

  -- con USING
SELECT
  *
FROM equipo e JOIN ciclista c
    USING (nomequipo)
;

  -- tb puede usarse WHERE xo NO ES RECOMENDABLE xa no liarnos con futuras consultas con COMBINACIONES INTERNAS + SELECCIONES (mejor xa filtrar resultados)
SELECT
  *
FROM equipo e JOIN ciclista c
    WHERE e.nomequipo = c.nomequipo     -- x.x
;

        /*
          PRODUCTO CARTESIANO
            -- todos con todos!!
        */
        
        -- NUNCA se utiliza en SQL
        SELECT * FROM ciclista c,equipo e  -- AVISA DE UN ERROR x repeticion de campos, NOMEQUIPO en este caso
        ;

        -- para CONVERTIR un PRODUCTO CARTESIANO en un INNER JOIN usariamos WHERE
        SELECT * FROM ciclista c,equipo e WHERE c.nomequipo=e.nomequipo
        ;


/*
  Listarme los nombre del ciclista y del equipo de aquellos ciclistas que hayan ganado puertos
*/
SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM
  ciclista c
    JOIN puerto p
      ON c.dorsal = p.dorsal
;

/*
  Listarme los nombre del ciclista y del equipo de aquellos ciclistas que hayan ganado etapas
*/
SELECT DISTINCT
  c.nombre,
  c.nomequipo
FROM
  ciclista c
    JOIN etapa e
      USING(dorsal)
;

  /* optimizando la consulta */
  -- c1 (subconsulta)
    SELECT DISTINCT e.dorsal FROM etapa e;

  -- completa
    SELECT c.nombre,c.nomequipo FROM ciclista c JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 USING(dorsal);

/* Ciclistas que han ganado puertos y el numero
   de puertos que han ganado. Del ciclista quiere
   saber el dorsal y el nombre
   dorsal, nombre, numeroPuertos
*/
-- c1
SELECT p.dorsal, c.nombre, p.nompuerto
FROM puerto p JOIN ciclista c USING(dorsal)
;

-- completa
SELECT
  * 
FROM (
  SELECT p.dorsal, c.nombre, p.nompuerto
  FROM puerto p JOIN ciclista c USING(dorsal)
) c1
GROUP BY c1.dorsal
;