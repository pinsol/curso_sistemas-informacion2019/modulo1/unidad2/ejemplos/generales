﻿                                          /** MÓDULO I - UNIDAD 2 - Ejemplo Teoria1 **/

                                      /* más ejemplos de repaso, ahora con VIEWS (VISTAS) */

USE teoria1;

/* 1. Indicar el nombre de las marcas que se hayan alquilado */
CREATE OR REPLACE VIEW consulta1 AS
  SELECT
    DISTINCT c.marca
  FROM alquileres a
    JOIN coches c ON a.coche = c.codigoCoche
;

CREATE OR REPLACE VIEW cochesAlquilados AS
  SELECT
    DISTINCT a.coche 
  FROM
    alquileres a
;

  -- optimizada
  SELECT
    DISTINCT c.marca 
  FROM 
    cochesAlquilados
    JOIN coches c ON cochesAlquilados.coche = c.codigoCoche
  ;

/* 2. Nombres de los usuarios que hayan alquilado alguna vez coches */
CREATE OR REPLACE VIEW usuariosAlquilados AS
  SELECT
    DISTINCT a.usuario usuarios 
  FROM
    alquileres a
;

SELECT
  u.nombre 
FROM
  usuariosAlquilados a 
JOIN usuarios u ON u.codigoUsuario = a.usuarios
;

/* 3. coches que NO han sido alquilados */
CREATE OR REPLACE VIEW codCochesAlquilados AS
   SELECT
     DISTINCT a.coche codigoCoche 
   FROM
     alquileres a
;

SELECT
  c.codigoCoche 
FROM
  coches c 
LEFT JOIN
  codCochesAlquilados ca USING (codigoCoche)
WHERE
  ca.codigoCoche IS NULL
;