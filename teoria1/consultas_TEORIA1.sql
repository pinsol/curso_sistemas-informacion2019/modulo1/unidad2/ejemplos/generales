﻿                                          /** MÓDULO I - UNIDAD 2 - Ejemplo Teoria1 **/

                                                    /* ejemplos de repaso */

USE teoria1;

/* 1. Cuántos coches ha alquilado el usuario 1 */
  -- 2 veces el mismo coche (coche 1)
SELECT
  COUNT(*) nAlquileres    -- numero de alquileres
FROM
  alquileres a
WHERE
  a.usuario = 1
;

SELECT
  COUNT(DISTINCT a.coche) nCoches   -- numero de coches
FROM
  alquileres a
WHERE
  a.usuario = 1
;

/* 2. Numero de alquileres que hay por mes */
-- 5 en el mes 7
SELECT
  MONTH(a.fecha) mes,
  COUNT(*) nAlquileres
FROM
  alquileres a
    GROUP BY
      MONTH(a.fecha)
;

/* 3. Número de usuarios por sexo */
-- 3 hombres, 3 mujeres, 1 null
SELECT
  u.sexo,
  COUNT(*) n
FROM
  usuarios u
    GROUP BY u.sexo
;

/* 4. Número de alquileres de coches por color */
-- 5 coches rojos alquilados
SELECT
  c.color,
  COUNT(*) n
FROM
  alquileres a
  JOIN coches c ON a.coche = c.codigoCoche
    GROUP BY c.color
;

  -- todos los colores
  SELECT
    c.color,
    COUNT(a.codigoAlquiler) n                         -- contaría nulos
  FROM
    alquileres a
    RIGHT JOIN coches c ON a.coche = c.codigoCoche    -- con un RIGHT JOIN
      GROUP BY c.color
  ;

/* 5. Número de marcas */
-- 3 (renault, ford, toyota)
SELECT
  COUNT(DISTINCT c.marca) nMarcas
FROM
  coches c
;

/* 6. Número de marcas de coches alquilados */
-- 2 (renault, ford)
SELECT
  COUNT(DISTINCT c.marca) nMarcasAlquiladas
FROM
  alquileres a
  JOIN coches c ON a.coche = c.codigoCoche
;