﻿                                          /** MÓDULO I - UNIDAD 2 - Ejemplo Teoria1 **/

                                                    /* más ejemplos de repaso */

USE teoria1;

/* 1. Indicar el nombre de las marcas que se hayan alquilado */
SELECT
  DISTINCT c.marca
FROM alquileres a
  JOIN coches c ON a.coche = c.codigoCoche
;

  -- optimizada
  SELECT
    DISTINCT c.marca 
  FROM 
    (SELECT DISTINCT a.coche FROM alquileres a) c1
    JOIN coches c ON c1.coche = c.codigoCoche
  ;

/* 2. Nombres de los usuarios que hayan alquilado alguna vez coches */
SELECT
  u.nombre 
FROM 
  (
    SELECT
      DISTINCT a.usuario codigoUsuario 
    FROM
      alquileres a
  ) c1 
JOIN usuarios u USING (codigoUsuario)
;

/* 3. coches que NO han sido alquilados */
SELECT
  c.codigoCoche 
FROM
  coches c 
LEFT JOIN
  (
    SELECT DISTINCT a.coche FROM alquileres a
  ) c1 ON c.codigoCoche = c1.coche 
WHERE
  c1.coche IS NULL
;