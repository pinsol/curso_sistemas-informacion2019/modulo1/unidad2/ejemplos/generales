﻿                                                                      /** EJEMPLOS PROYECCIONES **/
-- cambiar base de datos  ¡¡IMPORTANTE!!
USE ciclistas;

/* Listar todos los nombres de los ciclistas */
SELECT DISTINCT c.nombre FROM ciclista AS c; -- el alias se pone con AS c
SELECT DISTINCT c.nombre FROM ciclista c; -- no hace falta el AS
SELECT DISTINCT ciclista.nombre FROM ciclista; -- tabla.campo (sin alias)

SELECT  -- tabuleamos la consulta para una mejor lectura
  DISTINCT nombre  -- la proyeccion incluye necesariamente el DISTINCT ya que indica SIN DUPLICADOS
FROM  -- elige TABLA
  ciclista -- no hace falta alias y al ser solamente una tabla no hay equivocacion alguna que el campo sea de ella
; -- cierre de consulta
