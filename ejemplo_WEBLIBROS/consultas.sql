﻿/**
                            MÓDULO I - UNIDAD 2 - EJEMPLO WEB-LIBROS
                              Jandro, Roberto y Jose  
**/

USE paginaweb;

/* 1. Título del libro y nombre del autor que lo ha escrito */
SELECT
  l.titulo, a.nombre_completo 
FROM
  libro l 
  JOIN autor a ON l.autor = a.id_autor
;

/* 2. Nombre del autor y el título del libro en el que han ayudado */
SELECT
  a.nombre_completo ayudante,
  l.titulo libroColabora
FROM
  ayuda
  JOIN autor a ON ayuda.autor = a.id_autor
  JOIN libro l ON a.id_autor = l.autor
;

/*
   3. Los libros que se ha descargado cada usuario con la fecha de descarga.
   Listar el login, fecha descarga, id_libro
*/
SELECT
  *
FROM
  fechadescarga
;

/* 
   4. Los libros que se ha descargado cada usuario con la fecha de descarga.
   Listar el login, correo, fecha descarga, titulo del libro
*/
SELECT
  u.login,
  u.email,
  f.fecha_descarga,
  l.titulo
FROM fechadescarga f
  JOIN descarga d ON f.libro = d.libro AND f.usuario = d.usuario
  JOIN usuario u  ON d.usuario = u.login
  JOIN libro l ON d.libro = l.id_libro
;

  -- juntando 'JOINs' y 'ONs'     ¡NO RECOMENDABLE!
  SELECT
    u.login,
    u.email,
    f.fecha_descarga,
    l.titulo
  FROM fechadescarga f
    JOIN descarga d 
    JOIN usuario u  
    JOIN libro l 
  
    ON f.libro = d.libro AND f.usuario = d.usuario
    AND d.usuario = u.login
    AND d.libro = l.id_libro
  ;

/* 5. El numero de libros que hay */
SELECT
  COUNT(*) nLibros
FROM
  libro l
;

/*
  6. El número de libros por colección
  (no hace falya colocar nombre de la colección)
*/
SELECT
  l.coleccion,
  COUNT(l.id_libro) nLibros
FROM
  libro l
    GROUP BY l.coleccion
;

/*
  7. La colección que tiene más libros
  (no hace falta nombre de la colección)
*/
  -- consulta C1
  SELECT
    l.coleccion,COUNT(*) nLibros
  FROM
    libro l
      GROUP BY l.coleccion
  ;

  -- consulta c2
SELECT
  MAX(c1.nLibros) maximo
FROM
(
  SELECT
      l.coleccion,COUNT(*) nLibros
  FROM
      libro l
       GROUP BY l.coleccion
) c1
;

-- consulta completa
SELECT
  c1.coleccion
FROM
  (
    SELECT
        l.coleccion,COUNT(*) nLibros
    FROM
        libro l
         GROUP BY l.coleccion
  ) c1
JOIN 
  (
    SELECT
        MAX(c1.nLibros) maximo
    FROM
        (
          SELECT
            l.coleccion,COUNT(*) nLibros
          FROM
            libro l
              GROUP BY l.coleccion
        ) c1
  ) c2 ON c1.nLibros = c2.maximo
;

-- con WHERE

  -- final con WHERE
  SELECT * FROM 
    (
      SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
    ) c1
    WHERE nlibros=
      (
        SELECT MAX(c1.nlibros) maximo FROM 
        (
          SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      );

  -- final con HAVING
    SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion
      HAVING nlibros=
      (
        SELECT MAX(c1.nlibros) maximo FROM 
        (
          SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion
        ) c1
      );


/*
  8. La colección que tiene menos libros
  (no hace falta nombre de la colección)
*/
  -- consulta C1
  SELECT
    l.coleccion,COUNT(*) nLibros
  FROM
    libro l
      GROUP BY l.coleccion
  ;

  -- consulta c2
SELECT
  MIN(c1.nLibros) minimo
FROM
(
  SELECT
      l.coleccion,COUNT(*) nLibros
  FROM
      libro l
       GROUP BY l.coleccion
) c1
;

-- consulta completa
SELECT
  c1.coleccion
FROM
  (
    SELECT
        l.coleccion,COUNT(*) nLibros
    FROM
        libro l
         GROUP BY l.coleccion
  ) c1
JOIN 
  (
    SELECT
        MIN(c1.nLibros) minimo
    FROM
        (
          SELECT
            l.coleccion,COUNT(*) nLibros
          FROM
            libro l
              GROUP BY l.coleccion
        ) c1
  ) c2 ON c1.nLibros = c2.minimo
;

/* 9. El nombre de la colección que tiene más libros */
SELECT
  c.nombre
FROM
  (
    SELECT
        l.coleccion,COUNT(*) nLibros
    FROM
        libro l
         GROUP BY l.coleccion
  ) c1
JOIN 
  (
   SELECT
        MAX(c1.nLibros) maximo
   FROM
        (
         SELECT
            l.coleccion,COUNT(*) nLibros
         FROM
            libro l
              GROUP BY l.coleccion
        ) c1
  ) c2 ON c1.nLibros = c2.maximo
JOIN coleccion c ON c1.coleccion = c.id_coleccion 
;

/* 10. El nombre de la coleccion que tiene menos libros */
SELECT
  c.nombre
FROM
  (
    SELECT
        l.coleccion,COUNT(*) nLibros
    FROM
        libro l
         GROUP BY l.coleccion
  ) c1
JOIN 
  (
    SELECT
        MIN(c1.nLibros) minimo
    FROM
        (
          SELECT
            l.coleccion,COUNT(*) nLibros
          FROM
            libro l
              GROUP BY l.coleccion
        ) c1
  ) c2 ON c1.nLibros = c2.minimo
JOIN coleccion c ON c1.coleccion = c.id_coleccion
;

/* 11. El nombre del libro que se ha descargado más veces */
SELECT l.titulo FROM 
  (SELECT c1.libro
  FROM
    (
    SELECT
      f.libro, COUNT(*) nDescargas
    FROM
      fechadescarga f
        GROUP BY f.libro
   ) c1
  JOIN
   (SELECT MAX(c1.nDescargas) maximo FROM 
   (
    SELECT
      f.libro, COUNT(*) nDescargas
    FROM
      fechadescarga f
        GROUP BY f.libro
   ) c1
  ) c2 ON c1.nDescargas = c2.maximo) c3
JOIN libro l ON c3.libro = l.id_libro 
;

/* 12. El nombre del usuario que ha descargado más libros */
SELECT c1.usuario
  FROM
    (
    SELECT
      f.usuario, COUNT(*) nDescargas
    FROM
      fechadescarga f
        GROUP BY f.usuario
   ) c1
  JOIN
   (SELECT MAX(c1.nDescargas) maximo FROM 
   (
    SELECT
      f.usuario, COUNT(*) nDescargas
    FROM
      fechadescarga f
        GROUP BY f.usuario
   ) c1
  ) c2 ON c1.nDescargas = c2.maximo 
;

/* 13. El nombre de los usuarios que han descargado más libros que Adam3 */
SELECT
  c2.usuario 
FROM 
  (
  SELECT COUNT(*) descargasAdam3 FROM fechadescarga f WHERE f.usuario = 'Adam3'
  ) c1
JOIN
  (
  SELECT f.usuario,COUNT(*) nDescargas FROM fechadescarga f GROUP BY f.usuario
  ) c2
ON c1.descargasAdam3 = c2.nDescargas
WHERE c2.usuario <> 'Adam3'   
;


/* 14. El mes que más libros se han descargado */
SELECT c3.mes 
FROM
  (SELECT
  MAX(c1.nDescargas) maximo
  FROM 
  (
    SELECT MONTH(f.fecha_descarga) mesMASdescargado,COUNT(*) nDescargas FROM fechadescarga f GROUP BY MONTH(f.fecha_descarga)
  ) c1
  ) c2
JOIN 
  (
    SELECT MONTH(f.fecha_descarga) mes,COUNT(*) nDescargas FROM fechadescarga f GROUP BY MONTH(f.fecha_descarga)
  ) c3
ON c2.maximo = c3.nDescargas
;